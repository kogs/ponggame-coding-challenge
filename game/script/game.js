var gamefield;
var ball;
var leftPaddle;
var rightPaddle;
var scorePlayer1Text;
var scorePlayer2Text;

function initGame(){
	gamefield = document.getElementById("gamefield");
	ball = document.getElementById("ball");
	leftPaddle = document.getElementById("leftPaddle");
	rightPaddle = document.getElementById("rightPaddle");
	scorePlayer1Text = document.getElementById("player1Score");
	scorePlayer2Text = document.getElementById("player2Score");
	gamefield.onmousemove  = handleMouseMove;
	initKI();
}
function startGame(){
		stopGame();
		//TODO Wait for last update finished
		running = true;
		lastFrameTime = Date.now();
		realBallX = gamefield.offsetWidth / 2;
		realBallY = gamefield.offsetHeight / 2;
		stepX = -1;
		setpY = 0;
		speed = 1;
		update();
	
}
function pauseGame(){
	if(running){
		running = false;
	}else{
		running = true;
		update();
	}
}

function stopGame(){
	running = false;
	speed = 0;
	player1Score = 0;
	player2Score = 0;
	leftPaddle.style.top = 0+"px";
	rightPaddle.style.top = 0+"px";
	realBallX = 0;
	realBallY = 0;
}
	
var player1Score = 0;
var player2Score = 0;
	
var running = false;
var lastFrameTime;
var stepX = 1;
var stepY = -0;
var speed = 1;
var maxXSpeed = 8;
var maxXSpeedNeg = maxXSpeed * -1;

var realBallX = 150.0;
var realBallY = 50.0;

//the engine update
function update() {
	
	//some delta time stuff here
	var now = Date.now();
	var deltaTime = now - lastFrameTime;
	lastFrameTime = now;
	var gameFieldWidth = gamefield.offsetWidth;
	var gameFieldHeight = gamefield.offsetHeight;
	
	var ballLeftPos = ball.offsetLeft + 0.0,
            ballTopPos = ball.offsetTop + 0.0;
	
	//check box hits
	if(ballLeftPos < 0){
		player2Score++;
		speed = 1;
		stepX = -1;
		stepY = 0;
		realBallX = gameFieldWidth / 2;
		realBallY = gameFieldHeight /2;
	}else if( ballLeftPos > gameFieldWidth){
		player1Score++;
		speed = 1;
		stepX = 1;
		stepY = 0;
		realBallX = gameFieldWidth / 2;
		realBallY = gameFieldHeight /2;
	}
	
	if(ballTopPos < 0 || ballTopPos > gameFieldHeight){
		stepY *= -1;
	}
	
	if((realBallX >= leftPaddle.offsetLeft && realBallX <= (leftPaddle.offsetLeft + leftPaddle.offsetWidth)) && 
	(realBallY >= leftPaddle.offsetTop && realBallY <= (leftPaddle.offsetTop + leftPaddle.offsetHeight))){
		stepX *= -1;
		speed += 0.5;
		var baseY = realBallY - leftPaddle.offsetTop;
		stepY = (baseY / (leftPaddle.offsetHeight/2))-1;
	}
	
	if((realBallX >= rightPaddle.offsetLeft && realBallX <= (rightPaddle.offsetLeft + rightPaddle.offsetWidth)) && 
	(realBallY >= rightPaddle.offsetTop && realBallY <= (rightPaddle.offsetTop + rightPaddle.offsetHeight))){
		stepX *= -1;
		speed += 0.5;
		var baseY = realBallY - rightPaddle.offsetTop;
		stepY = (baseY / (rightPaddle.offsetHeight/2))-1;
		kiCalcOffset();
	}
	if(speed > 0 && speed > maxXSpeed){
		speed = maxXSpeed;
	}else if(speed < 0 && speed < maxXSpeedNeg){
		speed = maxXSpeedNeg;
	}
	console.log(speed);
	realBallX += stepX * speed;
	realBallY += stepY *( speed/2);
	
	ball.style.top =  realBallY - (ball.offsetHeight/2) + "px";
	ball.style.left = realBallX - (ball.offsetWidth/2) +"px";
	
	leftPaddle.style.top = (relMouseY - leftPaddle.offsetHeight / 2) + "px";
	kiUpdate();
	
	scorePlayer1Text.innerHTML = player1Score;
	scorePlayer2Text.innerHTML = player2Score;
	
	if(running){
		setTimeout(update, 15-deltaTime);
	}
}
//Stuff for mouse
var relMouseX = 0;
var relMouseY = 0;
function handleMouseMove(event) {
			relMouseX = event.pageX - gamefield.offsetLeft;
            relMouseY = event.pageY - gamefield.offsetTop;
}

//ki
var radioKIEasy;
var radioKINormal;
var radioKiHard;

function initKI(){
	radioKIEasy = document.getElementById("easyKi");
	radioKINormal = document.getElementById("normalKi");
	radioKiHard = document.getElementById("hardKi");
	radioKINormal.checked = true;
	kiCalcOffset();
}

var kiOffset = 0;

function kiUpdate(){
	if(radioKIEasy.checked){
		if(realBallY > rightPaddle.offsetTop+kiOffset){
			rightPaddle.style.top = rightPaddle.offsetTop + 1 +  "px";
		}else{
			rightPaddle.style.top = rightPaddle.offsetTop - 1  + "px";
		}
	}else if(radioKINormal.checked){
		if(realBallY > rightPaddle.offsetTop+kiOffset){
			rightPaddle.style.top = rightPaddle.offsetTop + 2 + "px";
		}else{
			rightPaddle.style.top = rightPaddle.offsetTop - 2 +"px";
		}
	}else if(radioKiHard.checked){
		rightPaddle.style.top = realBallY - kiOffset + "px";
	}
}

function kiCalcOffset(){
	if(radioKIEasy.checked){
		kiOffset = rightPaddle.offsetHeight/2;
	}else if(radioKINormal.checked){
		kiOffset = getRandomInt(5,rightPaddle.offsetHeight-10);
	}else if(radioKiHard.checked){
		kiOffset = getRandomInt(5,rightPaddle.offsetHeight-10);
	}
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}




